FreeNAS Cinder Driver
=====================

This repository is based on the Official iXsystems FreeNAS Cinder Driver Repo: https://github.com/iXsystems/cinder
Porting work has been done in order to make the driver compatible with the Cinder implementation Openstack Train and Python3.

The original driver located on the official iXsystems Github Repo is only compatibile with Python 2.7. 

Requirements
============

1. A FreeNAS system with at least 8 Gb of memory and a minimum 20 GiB disk.
2. An Openstack deployment running the Train Release or higher.
3. Python 3+


Getting Started
===============

Download and install the FreeNAS Cinder driver on the system running the Cinder API:

```
% sudo -s
# cd /opt
# git clone --depth=1 https://gitlab.com/hbonath/ixsystems-freenas-cinder.git
# cp -Rv ./ixsystems-freenas-cinder/driver/ixsystems /path/to/cinder/volume/drivers/ # substitute your own path here
```

Configure the Cinder driver. Open **/etc/cinder/cinder.conf** in an editor to both *edit* and *add* parameters.

**Edit these lines**:

 ```
 default_volume_type = freenas
 enabled_backends = freenas, lvmdriver-1
 ```

**Add these parameters and the appropriate values**:

 ```
 [freenas]
 iscsi_helper = <iscsi helper type. Standard Value>
 volume_dd_blocksize = <block size>
 volume_driver = <Path of the main class of iXsystems cinder driver. The standard value for this driver is cinder.volume.drivers.ixsystems.iscsi.FreeNASISCSIDriver>
 ixsystems_login = <username of FreeNAS Host>
 ixsystems_password = <Password of FreeNAS Host>
 ixsystems_server_hostname = <IP Address of FreeNAS Host>
 ixsystems_volume_backend_name = <Displays as 'Type' in Horizon. Standard value is 'freenas' >
 ixsystems_iqn_prefix = <Base name of ISCSI Target. (Get it from the web UI of the connected FreeNAS system by navigating: Sharing -> Block(iscsi) -> Target Global Configuration -> Base Name)>
 ixsystems_datastore_pool = <Create a dataset on the connected FreeNAS host and assign the dataset name here as a value. e.g. 'cinder-tank'>
 ixsystems_vendor_name = <driver specific information. Standard value is 'iXsystems' >
 ixsystems_storage_protocol =  <driver specific information. Standard value is 'iscsi'>
 ```

Here is an example configuration:

 ```
[freenas]
iscsi_helper = tgtadm
ixsystems_datastore_pool = datavol
ixsystems_iqn_prefix = iqn.2005-10.org.freenas.ctl
ixsystems_login = root
ixsystems_password = FREENASROOTPASSWORD
ixsystems_server_hostname = 198.51.100.25
ixsystems_storage_protocol = iscsi
ixsystems_vendor_name = iXsystems
ixsystems_volume_backend_name = freenas
volume_backend_name = freenas
volume_dd_blocksize = 512
volume_driver = cinder.volume.drivers.ixsystems.iscsi.FreeNASISCSIDriver
 ```

Now restart the Cinder Volume service to enable the changes. e.g. `systemctl restart cinder-volume.service`

After the Cinder service is restarted, log in to the Horizon interface of your Openstack deployment in a web browser. After logging in, navigate to `Admin -> System -> Volumes -> Volume Types` and click `Create Volume Type`. Type `freenas` in the **Name** field and check the **Public** option. Create this volume type, which is added to the list of types after the system completes the task. Now the FreeNAS Cinder driver is functional in the OpenStack Web Interface.

Using the Cinder Driver
=======================

Here are some examples commands:

* Create a volume:

  `(openstack-cli) $ openstack volume create --type freenas --size <volumeSizeInGiB> --description <volumeDescription> <volumeName>`

  Example:

  `(openstack-cli) $ openstack volume create --type freenas --size 8 --description Example My-First-FreeNAS-Volume`

The `Projects -> Volumes` and `Admin -> Volumes` sections of the web interface integrate Cinder functionality directly in the interface options using the `freenas` Volume Type.

About Source Code
=================

The FreeNAS Cinder driver uses several scripts:

* **iscsi.py**: Defines the Cinder APIs, including `create_volume` and `delete_volume`.
* **freenasapi.py**: Defines the REST API call routine and methods.
* **options.py**: Defines the default configuration parameters not fetched from the **cinder.conf** file.
